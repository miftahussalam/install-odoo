#!/bin/bash

sudo mv odoo-server.conf /etc
sudo chmod +r /etc/odoo-server.conf

sudo apt-get install python-pip python-dev libevent-dev gcc libxml2-dev node-less python-cups python-dateutil python-decorator python-docutils python-feedparser python-gdata python-geoip python-gevent python-imaging python-jinja2 python-ldap python-libxslt1 python-lxml python-mako python-mock python-openid python-passlib python-psutil python-psycopg2 python-pybabel python-pychart python-pydot python-pyparsing python-pypdf python-reportlab python-requests python-simplejson python-tz python-unicodecsv python-unittest2 python-vatnumber python-vobject python-werkzeug python-xlwt python-yaml wkhtmltopdf

sudo apt-get install postgresql
sudo su - postgres
createuser -s odoo
psql
ALTER USER odoo PASSWORD 'odoo';
\q
exit